require("dotenv").config();
const {
  Op
} = require("sequelize");

const {
  sequelize,
  model
} = require('../configs/database');

module.exports = {
  // ADD REVIEWS
  insert: async ( comment, rating, movie_id, user_id) => {
    return await model.Reviews.create({
      comment,
      rating,
      movie_id,
      user_id

    });
  },
};