const express = require("express");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
// const cors = require("cors")

const swaggerDocument = require("../swagger.json");
// define router
const router = require("./routes");

// define app 
const app = express();

// body parser to get data from body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//  import router here
app.use(router.moviesRouter);
app.use(router.userRouter);
app.use(router.reviewRouter);
app.use(router.uploadRouter);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// app.use(cors())

// export router back to index.js
module.exports = app;