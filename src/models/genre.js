const e = require("express");

module.exports = (sequelize, type) => {
    return sequelize.define(
      "movies",
      {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        genre: {
          type: type.STRING(255),
        }
      },
      {
        freezeTableName: true,
        underscored: true,
        
      }
    );
  };
  