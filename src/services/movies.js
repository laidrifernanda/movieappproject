require("dotenv").config();
const {
  Op
} = require("sequelize");

const {
  sequelize,
  model
} = require('../configs/database');

module.exports = {
  // ADD MOVIES
  insert: async (title, genre_id, release_date, director, budget, featured_song, synopsis, poster, trailer, backdrop) => {
    return await model.Movies.create({
      title,
      genre_id,
      release_date,
      director,
      budget,
      featured_song,
      synopsis,
      poster,
      trailer,
      backdrop
    });
  },
  //  GET MOVIES BY TITLE
  get: async ( title ) => {
    return await model.Movies.findAll({
      where: {
        title: {
          [Op.like]: '%' + title + '%'
        },
      },
      include: {
        model: model.Reviews
      }
    });
  },
  //  GET ALL MOVIES LIST
  getAll: async ( ) => {
    return await model.Movies.findAll({
      include: {
        model: model.Reviews
      }
    });
  },
  //  GET MOVIES BY ID
  getOne: async (id) => {
    return await model.Movies.findOne({
      where: {
        id: id
      },
      include: {
          model: model.Reviews
      }
    });
  },
};