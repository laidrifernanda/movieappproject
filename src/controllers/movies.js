require("dotenv").config();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// APLICATION FOR MOVIES LOOK AT DEPENDENCIES DIRECTORY
const application = require("../services/movies");

module.exports = {
// ADD MOVIES
  insert: async (req, res) => {
    const { title, genre_id, release_date, director, budget, featured_song, synopsis, poster, trailer, backdrop } = req.body

    // PLEASE USE THIS ROLES DATA TO AUTHENTICATE IF ONLY ADMIN CAN ADD MOVIES USER = 0 ADMIN = 1
    const roles = data.roles
    const addMovies = await application.insert(
      title,
      genre_id,
      release_date,
      director,
      budget,
      featured_song,
      synopsis,
      poster,
      trailer,
      backdrop
    )
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: "Success add movies data",
      data: {addMovies},
    });
  },
//  GET MOVIES COLLECTION LIST by title
  find: async (req, res) => {
    const { title } = req.params
    console.log(req.params)
    const findMovies = await application.get(
      title
    )
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: "Result",
      data: {findMovies},
    });
  },
//  get MOVIES COLLECTION LIST
  list: async (req, res) => {
    const findMovies = await application.getAll()
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: "Movies List",
      data: {findMovies},
    });
  },
//  get MOVIES by id
  detail: async (req, res) => {
    const { id } = req.params
    const getDetail = await application.getOne(id)
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: "Movies Detail",
      data: {getDetail},
    });
  },
};
