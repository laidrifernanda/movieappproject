const joi = require("joi");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const multerS3 = require("multer-s3");
require("dotenv").config();

// FILE FORMAT FILTER
    fileFilter = async (req, file, cb, res) => {
    const { authentication } = req.headers
    let payload = jwt.verify(authentication, process.env.SECRET_KEY_TOKEN)
    if (!payload.error) {
        if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
            cb(null, true);
        } else {
            cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
        }
        data = payload
    } else {
        res.status(500).json({
            isSuccessful: 500,
            error: true,
            error_message: payload.error,
            message: "token is not valid",
            data: [],
        });
    }
}
module.exports = fileFilter