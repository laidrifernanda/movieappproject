// DEPENDENCIES
require("dotenv").config();
const router = require("express").Router();
/* ----------------------- */
const authMiddleware = require("../middlewares/auth");
const reviews = require("../controllers/reviews");

// MOVIES
router.post("/review", authMiddleware.validateToken, reviews.insert);


module.exports = router;