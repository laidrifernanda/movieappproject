// DEPENDENCIES
require("dotenv").config();
const router = require("express").Router();
/* ----------------------- */
const authMiddleware = require("../middlewares/auth");
const movies = require("../controllers/movies");

// MOVIES
router.get("/search/:title", movies.find);
router.post("/movies", authMiddleware.validateToken, movies.insert);
router.get("/movies/:id", movies.detail);
router.get("/movies", movies.list);


module.exports = router;