// defines router
const moviesRouter = require("./moviesRouter");
const userRouter = require("./userRouter");
const reviewRouter = require("./reviewsRouter");
const uploadRouter = require("./uploadRouter");

// export router for server.js
module.exports = {
  moviesRouter,
  userRouter,
  reviewRouter,
  uploadRouter
};
