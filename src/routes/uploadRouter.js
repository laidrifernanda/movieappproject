//  DEPENDENCIES
require("dotenv").config();
const multer = require('multer');
const multerS3 = require('multer-s3')
const router = require("express").Router();
/* ----------------------- */
const authMiddleware = require("../middlewares/profile");
const upload = require("../controllers/auth");


// MOVIES
router.post("/upload", authMiddleware.single('images'), upload.upload);

module.exports = router;


