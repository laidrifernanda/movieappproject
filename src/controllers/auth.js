require("dotenv").config();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const multer = require('multer')
const AWS = require('aws-sdk')
const multerS3 = require('multer-s3');
const s3 = new AWS.S3();

const application = require("../services/auth");

module.exports = {
  // USER LOGIN 
  login: async (req, res) => {
    const {
      username,
      password
    } = req.body;
    let message = "";
    let data = {};

    // CHECK USERNAME
    const userExist = await application.findUser(username);
    if (userExist.length === 0) {
      message = "username is not exist!"
    }

    // data user if exist
    const userProfile = userExist[0].dataValues;
    // ! hashed password userExist from database
    const savedPassword = userProfile.password;

    // CHECK PASSWORD
    const comparePassword = await bcrypt.compare(
      password,
      savedPassword
    );

    // IF PASSWORD WRONG 
    if (comparePassword === false) {
      message = "password is incorrect";
    } else {
      // IF PASSWORD RIGHT GENERATE TOKEN
      const token = jwt.sign(userProfile, process.env.SECRET_KEY_TOKEN, {
        expiresIn: "300000s",
      });
      data = token;
    }
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: message,
      data: data
    });
  },
  // REGISTER
  insert: async (req, res) => {
    const {
      username,
      email,
      password,
      first_name,
      last_name
    } = req.body;

    const checkUsernameExist = await application.findUser(username);

    let message = "";
    if (checkUsernameExist.length !== 0) {
      // IF USERNAME EXIST
      message = "username already registered";
    } else {
      // IF USERNAME DOES NOT EXIST
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(password, salt);

      await application.insert(username, email, hashedPassword, first_name, last_name);
      message = "register sucsess";
    }

    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: message,
      data: [],
    });
  },
  // UPDATE USER BASIC INFORMATION
  update: async (req, res) => {
    const {
      email,
      password,
      first_name,
      last_name
    } = req.body
    const id = data.id;

    // IF TOKEN VALID USER CAN UPDATE PERSONAL INFORMATION
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const updateData = await application.updateUser(
      id,
      email,
      hashedPassword,
      first_name,
      last_name
    )
    res.send(updateData)
  },
  // UPLOAD USER PROFIL PICTURE
  upload: async (req,res) => {
      const id = data.id;
      const uploadS3 = multer({
        fileFilter,
        storage: multerS3({
          s3: s3,
          acl: "public-read",
          bucket: "mymoviesapp/profile_picture",
          metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname });
          },
          key: (req, file, cb) => {
            cb(null, Date.now().toString() + "-" + file.originalname);
          },
        }),
      });

      // IF TOKEN VALID USER CAN UPDATE PERSONAL INFORMATION
      const updateData = await application.updata(
        // id,
        images = req.file.location
      )
      res.send(images)
  }
};