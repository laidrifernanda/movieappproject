require("dotenv").config();

const {
  sequelize,
  model
} = require('../configs/database');

module.exports = {
  findUser: async (username) => {
    username.toUpperCase;
    return await model.Users.findAll({
      where: {
        username: username
      },
    });
  },

  insert: async (username, email, password, first_name, last_name) => {
    return await model.Users.create({
      username,
      email,
      password,
      first_name,
      last_name,
      role: 0
    });
  },

  updateUser: async (id, email, password, first_name, last_name) => {
    return await model.Users.update({
      email,
      password,
      first_name,
      last_name,
    }, {
      where: {
        id: id
      }
    });
  },

  updata: async (id, images) => {
    return await model.Users.update({
      images,
    }, {
      where: {
        id: id
      },
    });
  }
};