require("dotenv").config();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// APLICATION FOR MOVIES LOOK AT DEPENDENCIES DIRECTORY
const application = require("../services/reviews");

module.exports = {
// ADD REVIEWS
  insert: async (req, res) => {
    const {  comment, rating, movie_id } = req.body
    const user_id = data.id
    // 
    const addReview = await application.insert(
      comment,
      rating,
      movie_id,
      user_id
    )
    res.send({
      isSuccessful: 200,
      error: false,
      error_message: {},
      message: "Success add movies data",
      data: {addReview},
    });
  },
// UPDATE USER BASIC INFORMATION
  update: async (req, res) => {
    const { comment, rating, id } = req.body
    const user_id = data.id;
    
    const updateData = await application.updateUser(
        id,
        user_id,
        comment,
        rating,
    )
    res.send(updateData)
  }
};
