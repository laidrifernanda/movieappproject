const e = require("express");

module.exports = (sequelize, type) => {
    return sequelize.define(
      "reviews",
      {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        comment: {
          type: type.STRING(255),
        },
        rating: {
          type: type.INTEGER,
        },
        movie_id: {
          type: type.INTEGER,
        },
        user_id: {
          type: type.INTEGER,
        }
      },
      {
        freezeTableName: true,
        underscored: true
      }
    );
  };
  