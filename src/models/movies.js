const e = require("express");

module.exports = (sequelize, type) => {
    return sequelize.define(
      "movies",
      {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        title: {
          type: type.STRING(255),
        },
        genre_id: {
          type: type.INTEGER,
        },
        release_date: {
          type: type.DATE,
        },
        director: {
          type: type.STRING(255),
        },
        budget: {
          type: type.STRING(255),
        },
        featured_song: {
          type: type.STRING(255),
        },
        synopsis: {
          type: type.STRING(2000),
        },
        poster: {
          type: type.STRING(255)
        },
        trailer: {
          type: type.STRING(255)
        },
        backdrop: {
          type: type.STRING(255)
        }
      },
      {
        freezeTableName: true,
        underscored: true,
        
      }
    );
  };
  